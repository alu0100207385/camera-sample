package com.example.aaron.camera_sample;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.support.media.ExifInterface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import java.io.IOException;


public class MainActivity extends AppCompatActivity {

    private static final int CAMERA_REQUEST = 800;
    private static final int PERMISSIONS_REQUEST_CAMERA = 850;
    private String path;
    private Activity activity;
    private ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializes();
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!cameraPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
            } else {
                Toast.makeText(activity, "App does not have permits to camera device", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        findViewById(R.id.container).setVisibility(View.GONE);
        findViewById(R.id.mainScreen).setVisibility(View.VISIBLE);
        if (requestCode == CAMERA_REQUEST) {
            switch (resultCode) {
                case RESULT_OK:
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        String img = (String) extras.get("path");
                        Bitmap imageBitmap = BitmapFactory.decodeFile(img);
                        imageView.setImageBitmap(imageBitmap);
                    }
                    break;
                case RESULT_CANCELED:
                    Toast.makeText(activity, "Cancelled by user", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Toast.makeText(activity, "There was a problem with taking the picture. Try again, please", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case PERMISSIONS_REQUEST_CAMERA:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                            // permission denied, boo! Disable the
                            // functionality that depends on this permission.
                            Toast.makeText(activity, "You cannot use camera without this permission", Toast.LENGTH_SHORT).show();
                        }
                break;
        }
    }


    /**
     * Initializes vars and UI elements.
     */
    private void initializes() {
        activity = this;
        imageView = findViewById(R.id.picture);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cameraPermission()) {
                    displayCamera();
                } else {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
                }
            }
        });
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Check Camera permission.
     * @return True if app has camera permission, false otherwise.
     */
    private boolean cameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        } else {
            int permission = PermissionChecker.checkSelfPermission(activity, Manifest.permission.CAMERA);
            return permission == PermissionChecker.PERMISSION_GRANTED;
        }
    }

    /**
     * Displays camera preview.
     */
    public void displayCamera(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            findViewById(R.id.mainScreen).setVisibility(View.GONE);
            findViewById(R.id.container).setVisibility(View.VISIBLE);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, CameraFragment.newInstance())
                    .commit();
        } else {
            Utils.showToast(this, "API device is not compatible");
        }
    }

    /**
     * Displays picture taken.
     */
    public void displayResult(){
        findViewById(R.id.container).setVisibility(View.GONE);
        findViewById(R.id.mainScreen).setVisibility(View.VISIBLE);
        // Close fragment
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getSupportFragmentManager().beginTransaction()
                    .remove(CameraFragment.newInstance())
                    .commit();
        } else {
            getSupportFragmentManager().popBackStack();
        }
        // Display result if it is possible
        Bitmap img = rotateImage(setReducedImageSize());
        if (img != null) {
            imageView.setImageBitmap(img);
        }
    }

    /**
     * Adapt bitmap to the image view.
     * @return Bitmap image reduced.
     */
    private Bitmap setReducedImageSize(){
        int targetImageViewWidth = imageView.getWidth();
        int targetImageViewHeidht = imageView.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int cameraImageWidth = bmOptions.outWidth;
        int cameraImageHeight = bmOptions.outHeight;

        bmOptions.inSampleSize = Math.min(cameraImageWidth/targetImageViewWidth, cameraImageHeight/targetImageViewHeidht);
        bmOptions.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, bmOptions);
    }

    /**
     * If the image comes rotated must be fixed.
     * Source: https://www.youtube.com/watch?v=mZbL2ofcdP0
     * @param bitmap Image to rotate.
     * @return Bitmap rotated.
     */
    private Bitmap rotateImage(Bitmap bitmap){
        if (path == null){
            return null;
        }
        ExifInterface exifInterface;
        try {
            exifInterface = new ExifInterface(path);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            Matrix matrix = new Matrix();
            switch (orientation){
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(270);
                    break;
                default:
                    Toast.makeText(this, "Error! Orientation: " + orientation, Toast.LENGTH_SHORT).show();
                    break;
            }
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
