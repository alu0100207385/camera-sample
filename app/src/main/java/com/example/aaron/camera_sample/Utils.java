package com.example.aaron.camera_sample;

import android.app.Activity;
import android.app.ProgressDialog;
import android.widget.Toast;

public class Utils {

    /**
     * Shows a {@link Toast} on the UI thread.
     *
     * @param text The message to show
     */
    public static void showToast(final Activity activity, final String text) {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * To display a progress dialog
     *
     * @param style          Type of progress dialog style.
     * @param progressDialog ProgressDialog: ProgressDialog object.
     * @param activity       Current activity.
     */
    public static void showProgressDialog(final int style, final ProgressDialog progressDialog, final Activity activity) {
        if (progressDialog != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.setProgressStyle(style);
                    progressDialog.setCancelable(false);
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                }
            });
        }
    }


    /**
     * To hide and close current progress dialog.
     * @param progressDialog Progress dialog object.
     */
    public static void hideProgressDialog(final ProgressDialog progressDialog, Activity activity) {
        if (progressDialog != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.hide();
                    progressDialog.dismiss();
                }
            });
        }
    }
}
