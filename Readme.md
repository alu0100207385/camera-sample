# Camera sample

Camera sample app developed using Google Camera API 2.

## Getting Started

Open the project, update your dependencies if it was necessary, build and run.

### Prerequisites

It works on Android 5.0 (API level 21) or greater.

### Sources

* https://github.com/googlesamples/android-Camera2Basic
* https://developer.android.com/guide/topics/media/camera